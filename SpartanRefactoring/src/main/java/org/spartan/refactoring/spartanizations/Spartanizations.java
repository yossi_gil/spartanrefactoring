package org.spartan.refactoring.spartanizations;

import java.util.*;

import org.spartan.refactoring.wring.Trimmer;

/**
 * @author Boris van Sosin <code><boris.van.sosin [at] gmail.com></code> (v2)
 * @author Ofir Elmakias <code><elmakias [at] outlook.com></code> (original /
 *         30.05.2014) (v3)
 * @author Tomer Zeltzer <code><tomerr90 [at] gmail.com></code> (original /
 *         30.05.2014) (v3)
 * @since 2013/07/01
 */
public class Spartanizations {
  private static Spartanization[] all = { //
      new Trimmer(), //
      // new ForwardDeclaration(), //
      // new InlineSingleUse(), //
  };
  @SuppressWarnings("synthetic-access") //
  private static final Map<String, Spartanization> map = new HashMap<String, Spartanization>() {
    private static final long serialVersionUID = -8921699276699040030L;
    {
      for (final Spartanization s : all)
        put(s.getClass().getSimpleName(), s);
    }
  };
  private final Spartanization value;
  private Spartanizations(final Spartanization value) {
    this.value = value;
  }
  /**
   * @return Spartanization class rule instance
   */
  public Spartanization value() {
    return value;
  }
  /**
   * @param c Spartanization rule
   * @return Spartanization class rule instance
   */
  @SuppressWarnings("unchecked") //
  public static <T extends Spartanization> T findInstance(final Class<? extends T> c) {
    for (final Spartanization $ : all)
      if ($.getClass().equals(c))
        return (T) $;
    return null;
  }
  /**
   * @return Iteration over all Spartanization class instances
   */
  public static Iterable<Spartanization> allAvailableSpartanizations() {
    return new Iterable<Spartanization>() {
      @SuppressWarnings("synthetic-access") //
      @Override public Iterator<Spartanization> iterator() {
        return new Iterator<Spartanization>() {
          int next = 0;
          @Override public boolean hasNext() {
            return next < all.length;
          }
          @Override public Spartanization next() {
            return all[next++];
          }
          @Override public final void remove() {
            throw new IllegalArgumentException();
          }
        };
      }
    };
  }
  /**
   * Resets the enumeration with the current values from the preferences file.
   * Letting the rules notification decisions be updated without restarting
   * eclipse.
   */
  public static void reset() {
    map.clear();
    for (final Spartanization s : all)
      map.put(s.getClass().getSimpleName(), s);
  }
  /**
   * @param name the name of the spartanization
   * @return an instance of the spartanization
   */
  public static Spartanization get(final String name) {
    assert name != null;
    return map.get(name);
  }
  /**
   * @return all the registered spartanization refactoring objects
   */
  public static Iterable<Spartanization> all() {
    return map.values();
  }
  /**
   * @return all the registered spartanization refactoring objects names
   */
  public static Set<String> allRulesNames() {
    return map.keySet();
  }
}
